import { Component } from '@angular/core';
import { StorageService } from './storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  public nightValue;

  constructor(public storage: StorageService) {

  }
  ngOnInit() {

    this.getData("nightmode").then(nightValue => {
      if (nightValue) {
        document.body.classList.toggle('dark');
        document.getElementById("themeToggle").setAttribute("checked", "true");
      }

      // the rest of init code that depends on the value;

    });
    // If using a custom driver:
    // await this.storage.defineDriver(MyCustomDriver)

  }
  async getData(key) {
    return this.nightValue = await this.storage.get(key);
  }



  nightToggle() {
    var x = document.getElementById("themeToggle");

    if (this.nightValue) {
      this.storage.set("nightmode", false)
      document.body.classList.toggle('dark');

    }
    else {
      this.storage.set("nightmode", true)
      document.body.classList.toggle('dark');

    }
  }



}
