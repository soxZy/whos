import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { setTimeout } from 'timers';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit {
  //#region  hiddenthings
  readyHidden = false;
  playHidden = true;
  //#endregion
  //#region life
  i = Array;
  lifes = 3;
  //#endregion

  //#region quizz content
  listQuizz = [
    {
      "id": 1,
      "name": "Quizz Général",
      "profilePic": "assets/img/speakers/bear.jpg",
      "about": "Quizz contenant tout type de questions"
    },
    {
      "id": 2,
      "name": "Marques",
      "profilePic": "assets/img/speakers/cheetah.jpg",
      "about": "Quelle est la plus vielle marque ?",
      "questions": [
        {
          "name": "MacDonald",
          "date": 1955,
          "pic": "assets/img/brands/macdonald.png",
          "funfact": ""
        },
        {
          "name": "Rolex",
          "date": 1905,
          "pic": "assets/img/brands/rolex.png",
          "funfact": ""
        },
        {
          "name": "Adidas",
          "date": 1949,
          "pic": "assets/img/brands/adidas.png",
          "funfact": ""
        },
        {
          "name": "Nike",
          "date": 1971,
          "pic": "assets/img/brands/nike.png",
          "funfact": ""
        },
        {
          "name": "Coca Cola",
          "date": 1886,
          "pic": "assets/img/brands/cocacola.png",
          "funfact": ""
        },
        {
          "name": "Pepsi",
          "date": 1898,
          "pic": "assets/img/brands/pepsi.png",
          "funfact": ""
        },
        {
          "name": "Facebook",
          "date": 2004,
          "pic": "assets/img/brands/facebook.png",
          "funfact": ""
        },
        {
          "name": "Dr. Pepper",
          "date": 1885,
          "pic": "assets/img/brands/drpepper.png",
          "funfact": ""
        },
        {
          "name": "Microsoft",
          "date": 1975,
          "pic": "assets/img/brands/microsoft.png",
          "funfact": ""
        },
        {
          "name": "Apple",
          "date": 1976,
          "pic": "assets/img/brands/apple.png",
          "funfact": ""
        },
        {
          "name": "Ford",
          "date": 1903,
          "pic": "assets/img/brands/ford.png",
          "funfact": ""
        },
        {
          "name": "Nestlé",
          "date": 1905,
          "pic": "assets/img/brands/nestle.png",
          "funfact": ""
        },
        {
          "name": "KFC",
          "date": 1939,
          "pic": "assets/img/brands/kfc.png",
          "funfact": ""
        },
        {
          "name": "Burger King",
          "date": 1954,
          "pic": "assets/img/brands/burgerking.png",
          "funfact": ""
        },
        {
          "name": "Lego",
          "date": 1932,
          "pic": "assets/img/brands/lego.png",
          "funfact": ""
        },
        {
          "name": "Ikea",
          "date": 1943,
          "pic": "assets/img/brands/ikea.png",
          "funfact": ""
        },
        {
          "name": "Nintendo",
          "date": 1889,
          "pic": "assets/img/brands/nintendo.png",
          "funfact": ""
        },
        {
          "name": "Goodyear",
          "date": 1898,
          "pic": "assets/img/brands/goodyear.png",
          "funfact": ""
        },
        {
          "name": "Twitter",
          "date": 2006,
          "pic": "assets/img/brands/twitter.png",
          "funfact": ""
        },
        {
          "name": "Umbro",
          "date": 1924,
          "pic": "assets/img/brands/umbro.png",
          "funfact": ""
        },
        {
          "name": "Disney",
          "date": 1929,
          "pic": "assets/img/brands/disney.png",
          "funfact": ""
        },
        {
          "name": "Amazon.com",
          "date": 1995,
          "pic": "assets/img/brands/amazon.png",
          "funfact": ""
        },
        {
          "name": "Google",
          "date": 1998,
          "pic": "assets/img/brands/google.png",
          "funfact": ""
        },
        {
          "name": "H&M",
          "date": 1947,
          "pic": "assets/img/brands/hm.png",
          "funfact": ""
        },
        {
          "name": "Toyota",
          "date": 1933,
          "pic": "assets/img/brands/toyota.png",
          "funfact": ""
        },
        {
          "name": "L'Oreal",
          "date": 1909,
          "pic": "assets/img/brands/loreal.png",
          "funfact": ""
        },
        {
          "name": "Nivea",
          "date": 1911,
          "pic": "assets/img/brands/nivea.png",
          "funfact": ""
        },
        {
          "name": "Yahoo!",
          "date": 1995,
          "pic": "assets/img/brands/yahoo.png",
          "funfact": ""
        }
        ,
        {
          "name": "UPS",
          "date": 1907,
          "pic": "assets/img/brands/ups.png",
          "funfact": ""
        }
        ,
        {
          "name": "FedEx",
          "date": 1971,
          "pic": "assets/img/brands/fedex.png",
          "funfact": ""
        }
        ,
        {
          "name": "BMW",
          "date": 1916,
          "pic": "assets/img/brands/bmw.png",
          "funfact": ""
        }
        ,
        {
          "name": "Sony",
          "date": 1946,
          "pic": "assets/img/brands/sony.png",
          "funfact": ""
        }
        ,
        {
          "name": "Starbucks",
          "date": 1971,
          "pic": "assets/img/brands/starbucks.png",
          "funfact": ""
        },
        {
          "name": "Redbull",
          "date": 1987,
          "pic": "assets/img/brands/redbull.png",
          "funfact": ""
        },
        {
          "name": "Intel",
          "date": 1968,
          "pic": "assets/img/brands/intel.png",
          "funfact": ""
        },
        {
          "name": "Marlboro",
          "date": 1908,
          "pic": "assets/img/brands/marlboro.png",
          "funfact": ""
        },
        {
          "name": "Dell",
          "date": 1984,
          "pic": "assets/img/brands/dell.png",
          "funfact": ""
        },
        {
          "name": "Nikon",
          "date": 1917,
          "pic": "assets/img/brands/nikon.png",
          "funfact": ""
        },
        {
          "name": "Canon",
          "date": 1937,
          "pic": "assets/img/brands/canon.png",
          "funfact": ""
        }
      ]
    },
    {
      "id": 3,
      "name": "Youtubers",
      "profilePic": "assets/img/speakers/duck.jpg",
      "about": "Qui est le plus vieux youtuber ?",
      "questions": [
        {
          "name": "Squeezie",
          "date": 1996,
          "pic": "assets/img/youtuber/squeezie.png",
          "funfact": ""
        },
        {
          "name": "Cyprien",
          "date": 1989,
          "pic": "assets/img/youtuber/cyprien.png",
          "funfact": ""
        },
        {
          "name": "Norman",
          "date": 1987,
          "pic": "assets/img/youtuber/norman.png",
          "funfact": ""
        },
        {
          "name": "Terracid",
          "date": 1992,
          "pic": "assets/img/youtuber/terracid.png",
          "funfact": ""
        },
        {
          "name": "Laink",
          "date": 1992,
          "pic": "assets/img/youtuber/laink.png",
          "funfact": ""
        },
        {
          "name": "Tibo Inshape",
          "date": 1992,
          "pic": "assets/img/youtuber/tiboinshape.png",
          "funfact": ""
        },
        {
          "name": "Amixem",
          "date": 1991,
          "pic": "assets/img/youtuber/amixem.png",
          "funfact": ""
        },
        {
          "name": "Natoo",
          "date": 1985,
          "pic": "assets/img/youtuber/natoo.png",
          "funfact": ""
        }
      ]
    },
    {
      "id": 4,
      "name": "Personnalité",
      "profilePic": "",
      "about": "Qui est la plus vieille personnalité ?",
      "questions": [
        {
          "name": "Omar SY",
          "date": 1978,
          "pic": "assets/img/personnalite/omarsy.png",
          "funfact": ""
        },
        {
          "name": "Jean-Jacques Goldman",
          "date": 1951,
          "pic": "assets/img/personnalite/goldman.png",
          "funfact": ""
        },
        {
          "name": "Michel Cymes",
          "date": 1957,
          "pic": "assets/img/personnalite/michelcymes.png",
          "funfact": ""
        },
        {
          "name": "Jean Reno",
          "date": 1948,
          "pic": "assets/img/personnalite/jeanreno.png",
          "funfact": ""
        },
        {
          "name": "Antoine Griezmann",
          "date": 1991,
          "pic": "assets/img/personnalite/griezmann.png",
          "funfact": ""
        },
        {
          "name": "Sophie Marceau",
          "date": 1966,
          "pic": "assets/img/personnalite/smarceau.png",
          "funfact": ""
        },
        {
          "name": "Florence Foresti",
          "date": 1973,
          "pic": "assets/img/personnalite/florenceforesti.png",
          "funfact": ""
        },
        {
          "name": "Renaud",
          "date": 1952,
          "pic": "assets/img/personnalite/renaud.png",
          "funfact": ""
        },
        {
          "name": "Teddy Riner",
          "date": 1989,
          "pic": "assets/img/personnalite/teddyrinner.png",
          "funfact": ""
        },
        {
          "name": "Gad Elmaleh",
          "date": 1971,
          "pic": "assets/img/personnalite/gadelmaleh.png",
          "funfact": ""
        },
        {
          "name": "Nagui",
          "date": 1961,
          "pic": "assets/img/personnalite/nagui.png",
          "funfact": ""
        },
        {
          "name": "Dany Boon",
          "date": 1966,
          "pic": "assets/img/personnalite/danyboon.png",
          "funfact": ""
        },
        {
          "name": "Florent Pagny",
          "date": 1961,
          "pic": "assets/img/personnalite/florentpagny.png",
          "funfact": ""
        },
        {
          "name": "Jean Dujardin",
          "date": 1972,
          "pic": "assets/img/personnalite/jeandujardin.png",
          "funfact": ""
        },
        {
          "name": "Fabrice Luchini",
          "date": 1951,
          "pic": "assets/img/personnalite/fabriceluchini.png",
          "funfact": ""
        },
        {
          "name": "Nicolas Hulot",
          "date": 1955,
          "pic": "assets/img/personnalite/nicolashulot.png",
          "funfact": ""
        },
        {
          "name": "Stéphane Bern",
          "date": 1963,
          "pic": "assets/img/personnalite/stephanebern.png",
          "funfact": ""
        },
        {
          "name": "Yannick Noah",
          "date": 1960,
          "pic": "assets/img/personnalite/yannicknoah.png",
          "funfact": ""
        },
        {
          "name": "Matt Pokora",
          "date": 1985,
          "pic": "assets/img/personnalite/mattpokora.png",
          "funfact": ""
        },
        {
          "name": "Louane",
          "date": 1996,
          "pic": "assets/img/personnalite/louane.png",
          "funfact": ""
        },
        {
          "name": "Emmanuel Macron",
          "date": 1977,
          "pic": "assets/img/personnalite/emmanuelmacron.png",
          "funfact": ""
        },
        {
          "name": "Didier Deschamps",
          "date": 1968,
          "pic": "assets/img/personnalite/didierdeschamps.png",
          "funfact": ""
        },
        {
          "name": "Mylène Farmer",
          "date": 1961,
          "pic": "assets/img/personnalite/mylenefarmer.png",
          "funfact": ""
        },
        {
          "name": "Jean-Pierre Pernaut",
          "date": 1950,
          "pic": "assets/img/personnalite/jpp.png",
          "funfact": ""
        },
        {
          "name": "Franck Dubosc",
          "date": 1963,
          "pic": "assets/img/personnalite/franckdubosc.png",
          "funfact": ""
        },
        {
          "name": "Francis Cabrel",
          "date": 1953,
          "pic": "assets/img/personnalite/franciscabrel.png",
          "funfact": ""
        },
        {
          "name": "Patrick Bruel",
          "date": 1959,
          "pic": "assets/img/personnalite/patrickbruel.png",
          "funfact": ""
        },
        {
          "name": "Tony Parker",
          "date": 1982,
          "pic": "assets/img/personnalite/tonyparker.png",
          "funfact": ""
        },
        {
          "name": "Marion Cotillard",
          "date": 1975,
          "pic": "assets/img/personnalite/marioncotillard.png",
          "funfact": ""
        }
      ]
    },
    {
      "id": 5,
      "name": "Sportif",
      "profilePic": "",
      "about": "Qui est le plus vieux sportif ?",
      "questions": [
        {
          "name": "Cristiano Ronaldo",
          "date": 1985,
          "pic": "assets/img/sportif/cristianoronaldo.png",
          "funfact": ""
        },
        {
          "name": "Roger Federer",
          "date": 1981,
          "pic": "assets/img/sportif/rogerfederer.png",
          "funfact": ""
        },
        {
          "name": "Neymar",
          "date": 1992,
          "pic": "assets/img/sportif/neymar.png",
          "funfact": ""
        },
        {
          "name": "Usain Bolt",
          "date": 1986,
          "pic": "assets/img/sportif/usainbolt.png",
          "funfact": ""
        },
        {
          "name": "Antoine Griezmann",
          "date": 1991,
          "pic": "assets/img/sportif/antoinegriezmann.png",
          "funfact": ""
        },
        {
          "name": "Rafael Nadal",
          "date": 1986,
          "pic": "assets/img/sportif/rafaelnadal.png",
          "funfact": ""
        },
        {
          "name": "Tiger Woods",
          "date": 1975,
          "pic": "assets/img/sportif/tigerwoods.png",
          "funfact": ""
        },
        {
          "name": "Serena Williams",
          "date": 1981,
          "pic": "assets/img/sportif/serenawilliams.png",
          "funfact": ""
        },
        {
          "name": "Teddy Riner",
          "date": 1989,
          "pic": "assets/img/sportif/teddyrinner.png",
          "funfact": ""
        },
        {
          "name": "Stephen Curry",
          "date": 1988,
          "pic": "assets/img/sportif/stephencurry.png",
          "funfact": ""
        },
        {
          "name": "Novak Djokovic",
          "date": 1987,
          "pic": "assets/img/sportif/novakdjokovic.png",
          "funfact": ""
        },
        {
          "name": "Paul Pogba",
          "date": 1993,
          "pic": "assets/img/sportif/paulpogba.png",
          "funfact": ""
        },
        {
          "name": "Zlatan Ibrahimović",
          "date": 1981,
          "pic": "assets/img/sportif/zlatanibrahimovic.png",
          "funfact": ""
        },
        {
          "name": "Andy Murray",
          "date": 1987,
          "pic": "assets/img/sportif/andymurray.png",
          "funfact": ""
        },
        {
          "name": "Luis Suárez",
          "date": 1987,
          "pic": "assets/img/sportif/luissarez.png",
          "funfact": ""
        },
        {
          "name": "Karim Benzema",
          "date": 1987,
          "pic": "assets/img/sportif/karimbenzema.png",
          "funfact": ""
        },
        {
          "name": "Fernando Alonso",
          "date": 1981,
          "pic": "assets/img/sportif/fernandoalonso.png",
          "funfact": ""
        },
        {
          "name": "Sebastian Vettel",
          "date": 1987,
          "pic": "assets/img/sportif/sebastianvettel.png",
          "funfact": ""
        },
        {
          "name": "Lewis Hamilton",
          "date": 1985,
          "pic": "assets/img/sportif/lewishamilton.png",
          "funfact": ""
        },
        {
          "name": "Zinédine Zidane",
          "date": 1972,
          "pic": "assets/img/sportif/zinedine.png",
          "funfact": ""
        },
        {
          "name": "Tony Parker",
          "date": 1982,
          "pic": "assets/img/sportif/tonyparker.png",
          "funfact": ""
        },
        {
          "name": "Fabien Barthez",
          "date": 1971,
          "pic": "assets/img/sportif/barthez.png",
          "funfact": ""
        },
        {
          "name": "Didier Deschamps",
          "date": 1968,
          "pic": "assets/img/sportif/didierdeschamps.png",
          "funfact": ""
        },
        {
          "name": "Bixente Lizarazu",
          "date": 1969,
          "pic": "assets/img/sportif/lizarazu.png",
          "funfact": ""
        },
        {
          "name": "Pierre-Ambroise Bosse",
          "date": 1992,
          "pic": "assets/img/sportif/pierreambroise.png",
          "funfact": ""
        },
        {
          "name": "Martin Fourcade",
          "date": 1988,
          "pic": "assets/img/sportif/martinfourcade.png.png",
          "funfact": ""
        }
      ]
    }
  ];
  //#endregion

  //#region timer
  timerValue: number = 10;
  interval;
  //#endregion
  selectedQuiz;
  message = "Bonne chance !"
  currentScore = 0;
  highScore;
  backgroundValue = "default";

  firstAnswer = { name: 'MacDonald', date: 1955, pic: 'assets/img/brands/macdonald.png', funfact: '' };
  secondAnswer = { name: 'Nestlé', date: 1905, pic: 'assets/img/brands/nestle.png', funfact: '' };
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe((params: any) => {
      this.getSelectedQuiz(params.id);
    });


  }
  getSelectedQuiz(id) {
    this.listQuizz.forEach(quiz => {
      if (quiz.id == id) {
        this.selectedQuiz = quiz;
        this.highScore = localStorage.getItem("high-" + this.selectedQuiz.id);
        return this.selectedQuiz;
      }
    });

  }

  startQuiz() {
    this.currentScore = 0;
    this.lifes = 3;
    this.hideStuff();
    this.newQuestion();
  }

  startTimer() {

    this.timerValue = 10;
    this.interval = setInterval(() => {
      this.backgroundValue = "default";
      if (this.timerValue > 1) {
        this.timerValue--;
      } else {
        this.wrongAnswer();
      }
    }, 1000)

  }



  newQuestion() {

    clearInterval(this.interval);
    this.startTimer();

    var selectedQuizLength = this.selectedQuiz.questions.length;

    function getRandomint(max) {
      return Math.floor(Math.random() * (max - 0)) + 0;
    }

    this.firstAnswer = this.selectedQuiz.questions[getRandomint(selectedQuizLength)];
    this.secondAnswer = this.selectedQuiz.questions[getRandomint(selectedQuizLength)];
    while (this.firstAnswer == this.secondAnswer) {
      this.secondAnswer = this.selectedQuiz.questions[getRandomint(selectedQuizLength)];
    }
  }

  userAnswer(value) {
    if (value < 0) {
      this.backgroundValue = "yes";
      // bonne réponse pélo
      this.currentScore++;
      this.newQuestion();

    }
    else {
      this.wrongAnswer();
      // mauvaise réponse dommage
    }
  }

  wrongAnswer() {

    this.backgroundValue = "no";


    this.lifes--;
    if (this.lifes < 1) {
      this.endQuiz();
    }
    else {
      this.newQuestion();
    }

  }

  endQuiz() {
    clearInterval(this.interval);


    this.verifyHighScore();
    this.hideStuff();
  }

  verifyHighScore() {
    var high = localStorage.getItem("high-" + this.selectedQuiz.id);
    console.log(high)
    if (parseInt(high) >= this.currentScore) {
      this.message = "Dommage vous avez déjà fait mieux !"
    }
    else {
      localStorage.setItem("high-" + this.selectedQuiz.id, this.currentScore.toString());
      this.message = "Nouveau record ! ";
    }
  }
  // change the hidden variable
  hideStuff() {
    if (this.readyHidden) {
      this.readyHidden = false;
      this.playHidden = true;
    }
    else {
      this.readyHidden = true;
      this.playHidden = false;
    }
  }

}
