import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  quizz = [{
    "id": 2,
    "title": "Marques",
    "bestScore": ""
  },
  {
    "id": 3,
    "title": "Youtubers",
    "bestScore": ""
  }
  ];
  constructor(private router: Router) { }

  ngOnInit() {
    for (let index = 0; index < this.quizz.length; index++) {
      console.log(this.quizz[index]);
      this.quizz[index].bestScore = localStorage.getItem("high-" + this.quizz[index].id);
    }
    console.log(this.quizz);
  }



  goToQuizz(id) {
    this.router.navigate(['/quiz/' + id])
    console.log(id);
  }
}
